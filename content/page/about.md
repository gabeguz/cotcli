+++
date = "2016-05-05T15:26:56-04:00"
title = "about"

+++
Who
===

My name is Gabriel Guzman, and I've been using the Command Line Interface (CLI)
as my primary method of talking to my computers since the 80s.  I feel that the
CLI is an incredible tool that many people could benefit from if only they knew
what to do with it.

I'm on the internet: 
[https://keybase.io/gabeguz] [0]

What
====

Comfort On The Command Line is a guide to the Command Line Interface for
beginners.  It's my attempt to present the CLI in a non intimidating way for
people who would like to learn how to use it.

Where
=====

I'm currently living in Montreal, QC. 

When
====

I started the project in July of 2015 while working in a [co-working space in
Reno] [1].  I overheard some developers talking and one of them asked the question:
"How do I jump to the beginning of the line?"  And I realized I could answer
that question (Control + A) and pass on some command line knowledge at the same
time.  Since then I've been writing in my spare time, trying to make sure each
article is small and easy to digest, as well as friendly to beginners.

Why
===

To help people get comfortable using the Command Line Interface (CLI). Teaching
is also a great way to learn, so I expect I'll be learning quite a bit myself as
I write.


[0]: https://keybase.io/gabeguz
[1]: http://renocollective.com
