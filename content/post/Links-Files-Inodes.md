+++
date = "2016-12-05T12:13:14-04:00"
publishDate = "2016-12-05T12:13:14-04:00"
bigimg = ""
title = "Links, files, inodes"
subtitle = ""

+++

I need to issue a quick apology to anyone who has signed up via the subscribe
button on the website, it seems that I had misconfigured some software and so
new subscribers would get every single post emailed to them in one shot.  That
wasn't my intention and I've since fixed the problem.  Sorry about that.  

Moving on...

Last time we created an empty file and were making our way through the output of
the `ls` command, today we'll continue where we left off and talk about links,
and inodes.

In case you missed last time, you can catch up here:
https://www.cotcli.com/post/Files-Permissions-Binary/

We left off with the permissions section of the following output: 

```ksh
$ ls -l
total 0
-rw-r--r--  1 gabe  gabe  0 Oct 11 15:35 a-new-file
```

Today we'll start with that number "1" just after the last two dashes (`--`). 

This number tells you how many links there are to your file.  Why is this number
1, if we haven't created any links to this file?  The reason is because this
number really refers to the number of links to the inode that your file points
to.  What's an inode?  Glad you asked. 

An inode is the underlying representation of a file.  It is where the actual
data in your file lives, as well as most of the information about your file.
The owner, the group, the size, the last access and modification times, as well
as the permission information that we looked at last time.  All this is actually
stored in the inode and not in the file itself.  The only information that is stored in the
file itself, is the name (a-new-file) and a *link* to the inode for that file.
This is why we see a '1' in the number of links column, because it is really
telling us there is 1 link to the underlying inode.  


You can visualize it a bit like this: 

```
				FILE	    LINK  		INODE
-------------------------------------------	  -------------------------------
| File Name: a-new-file | Inode: 11692817 | ----> | Inode: 11692817             |
-------------------------------------------	  -------------------------------
						  | Permissions: 644		|
						  | Last Modified: Oct 11 15:35 |
						  | Number of links: 1		|
						  | File size: 0		|
						  | Blocks on disk:		|
						  | ...				|
						  -------------------------------

```

And, you can ask the `ls` command to tell you the inode number for your files,
by passing the `-i` flag.

Let's create a couple of links to our file and see what happens (make sure you
are still in the cotcli directory we created last time).  We'll use the `ln`
command to create both a hard link and a symbolic link:

```ksh
$ ln -s a-new-file a-symbolic-link-to-a-new-file
$ ln a-new-file a-hard-link-to-a-new-file
$ ls -li
total 0
1169281 -rw-r--r--  2 gabe  gabe   0 Nov  2 13:37 a-hard-link-to-a-new-file
1169281 -rw-r--r--  2 gabe  gabe   0 Nov  2 13:37 a-new-file
1169283 lrwxr-xr-x  1 gabe  gabe  10 Nov  8 11:36 a-symbolic-link-to-a-new-file -> a-new-file
```

We can see in this output that `a-new-file` and `a-hard-link-to-a-new-file` both
have the same inode (1169281).  These two file names now point to the exact same
inode, and so are effectively the same exact file.  You should also notice that
the link count has gone up to 2 for both.  If you delete either one, the other
one will still be there completely unaffected.  If you change the contents of
one, the other will also change.  The data won't be deleted from disk until the
link count goes to 0, when you erase all files that point to the same inode.  

Why doesn't the link we created with `ln -s` point to the same inode?  

`ln -s` creates what's called a symbolic link.  This is similar to creating a
'shortcut' in Windows or an 'alias' on Mac OS.  It is "an indirect pointer to a
file." [Stevens & Rago p 120] [S&R] It has its own inode, and If you erase the
original file, the symbolic link will no longer work.

In general, you want to be using symbolic links instead of hard links.  Symbolic
links have a few advantages over hard links.  

1. You can use symbolic links across file systems.  Most people these days only
   have one big partition on their computer so this is less of an issue.  Since
   hard links point to an inode, and inodes are only unique across a single
   partition, you can't hard link to something on another partition.
2. You can make a symbolic link to a directory.  Only the superuser (a topic for
   another day) can create hard links to directories.


How many links do you think there are to a new directory?

```ksh
$ mkdir a-new-dir
$ ls -l
total 4
drwxr-xr-x  2 gabe  gabe  512 Nov  2 13:37 a-new-dir
-rw-r--r--  1 gabe  gabe    0 Nov  2 13:37 a-new-file
```
Why are there two links to a new directory?  Because every new
directory has a hard link to itself created automatically, the special "." file
in a directory that points to itself.  The ".." file is also created
automatically which references the parent directory.  

A handy thing to know about all this, is that moving `mv` files is faster than
copying `cp` them (assuming you are moving them within the same partition).  The
reason for this is that `cp` (copy) copies all the data into a new inode, but
move (`mv`) just moves the link to the inode.  Knowing this can save a lot of time when
you are reorganizing your movie collection!

New Terms
=========
* inode - the underlying representation of a file.
* hard link - a pointer to the same inode as the original file.
* symbolic link - an alias to the original file with it's own inode.
* `cp` - program to copy a file.
* `mv` - program to move a file.
* `ls -i` - include inode information in directory listing.
* `ln` - program to create links.

[S&R]: http://www.apuebook.com/apue3e.html "Advanced Programming in the Unix Environment, Third Edition"
