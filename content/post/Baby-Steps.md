+++
bigimg = ""
date = "2016-05-30T14:15:54-04:00"
subtitle = "A look at some basic shell commands"
title = "Baby Steps"
description = ""
publishdate = "2016-05-30"
tags = ["prompt", "cli", "commands"]
categories = ["general"]
tracks = ["intro"]
commands = ["whoami", "pwd"]
+++

Hello fellow command line interface (cli) adepts! 
 
Last time we found our terminal programs, and today we're not afraid to use
them.  We probably should be, but we'll leave that discussion for another day.

As mentioned in my last post once we open our terminal program we're presented with
`$>` or `#>` or maybe `gabe@desktop:~$` or who knows what.  Technically, since
the prompt is configurable it could say anything.  When I was just a wee lad, I
had mine set to say:  

```dos
C:\ What is thy bidding? >
```

I kid you not.

In case you didn't leave your terminal program running all night, please go
open it now.  At this point you should see your prompt, and you should be able
to type stuff at your computer.  Try it!  Most things will cause the computer
to reply rudely that it has no idea what you mean.  Below are some examples of
me typing a command (that thing that follows the `$>` prompt) followed by the
computer's output.  Feel free to type these on your own system as we go (don't
forget to hit return after each one):   

First, I try saying hello: 

```ksh
$> hello
```

To which my system replies: 

```ksh
ksh: hello: not found 
```

Then I try asking how the computer is doing: 

```ksh
$> how are you doing?
```

The computer's reply: 

```ksh
ksh: how: not found
```

How about asking the computer to please do something: 

```ksh
$> do something please
```

Computer says: 

```ksh
ksh: syntax error: `do' unexpected
```

right, very helpful computer.  No wonder someone invented the mouse.  

The main problem we have is communication.  I don't know what the terminal
wants me to say, and the terminal doesn't know what I want it to do.  On top of
that there doesn't seem to be a way to ask it for help, or is there? 

```ksh
$> help
```

(If you type help, and get a bunch of text but no prompt, you should either
press the space bar to scroll the text until the prompt comes back, or press
'q' to quit out of the message and return to the prompt.)

On my OpenBSD system I get a *very* helpful message that actually seems quite
useful (I admit I'm kind of shocked).  Your system may not be as helpful, so
I'm sharing a link to the message I see:
[http://man.openbsd.org/OpenBSD-current/man1/help.1](http://man.openbsd.org/OpenBSD-current/man1/help.1))  

Even if you're not on an OpenBSD system, I recommend reading that.  Go ahead,
I'll wait.

That basically just gave you everything you need to know about the UNIX cli
(command line interface).  Thankfully, I'm going to pretend it didn't and carry
on as if I didn't just read that incredibly helpful manual page.

For the rest of this guide I'll use the following convention.  When you see two
lines like this: 

```ksh
$> whoami
gabe
```

The first line is what I type (after the `$>` prompt), and the second line (or
multiple lines) is what the computer replies.   So, in the example above, I
typed the command `whoami`, and the computer replied `gabe`.

So, we're at our prompt, and we've just read about some pretty cool things we
can do, lets try one: 

```ksh
$> pwd
/home/gabe
```

That 'prints working directory' - it's equivalent to looking in your file
manager to see what folder you are currently in.  That is normally read "slash
home slash gabe." If I were in the Taxes folder under Documents, `pwd` would
return: `/home/gabe/Documents/Taxes`.  The `pwd` command will always tell you
every folder above the one you are currently in.  On most UNIX systems the
`/home` (slash home) folder is where all user accounts live.  Each user of the
system should have a folder below `/home`.  For example if I have 3 users (gabe,
joebob, and ariadne) each of us would have a folder inside the `home` folder:

```ksh
/home/gabe 
/home/joebob
/home/ariadne
```

(Note: On a Mac, `/home` is empty and all user folders exist inside the `/Users`
directory) 

There are a plethora of commands available on a UNIX system, and we'll cover
several of them in the next few posts.

New Terms 
=========
- `whoami` - A program that returns the currently logged in user's account
  name. 

- `pwd` - A program that returns the current directory along with all of the
  directories above it. 

- `/home` (slash home) - A directory that contains directories for each user on
  a system.

- `/Users` (slash Users) - The Mac equivalent of `/home`.

- `help` - A program that (at least on my system) returns a very helpful
  message informing the user about the system.
