+++
bigimg = ""
date = "2016-06-13T16:24:13-04:00"
subtitle = "How to find out what a command can do"
title = "man(1)"
postdate="2016-06-13"
+++

UNIX systems have documentation built into them in the form of man pages.  You
read these man pages with the `man` command.  Type `man` followed by the manual
you would like to read.  

To bring up the manual page for the `ls` command you would type: 

```ksh
$> man ls
```

You should then see a nicely formatted manual that will tell you everything
you need to know about the `ls` command.  If for some reason, you don't get
that, here is a link to what I see:

http://man.openbsd.org/OpenBSD-current/man1/ls.1

At the top of the man page, you will see a couple of things that look something
like this: 

```man
LS(1)                      General Commands Manual                     LS(1)

NAME
     ls – list directory contents
```
[^1]

As a rule, when reading manual pages, the name of the command you are reading
about will be followed by a number inside of parenthesis.  This number '(1)' in
our example is the *section* of the manual you are currently reading.  If you
read the manual page for the man command (`$> man man`) it will describe the
various sections that exist on your system.

In general, just typing `man <command name>` will be enough to get you to
section 1 (general commands), which for now is probably what you want. 

# Navigation

Once you launch the `man` command, you will see a bunch of text on your screen.
As you read, you can scroll the text to read more by pushing the 'j' key (to
scroll one line at a time).  You can also "page down" by pressing the space bar.
If you need to go back up, you can scroll one line up with the 'k' key, or press
the 'g' key to jump to the beginning of the document, and the 'G' key to jump to
the end.  Using the arrow keys will also work for scrolling, but I recommend
getting used to using j + k to move up and down, this will come in handy
later. Pressing 'q' at any time will exit the `man` program and return you to
the prompt. 

# Read the name section

The name section should be  self explanatory.  It just has the name of the
command whose man page you are reading.

# Skip the synopsis

The synopsis is the section that is probably the most confusing to beginners.
It tells you all the ways you can call the command from the command line and all
the options that are supported. You probably won't get much from looking at this
right now which is why I recommend you skip it on your first read of a man page. 

# Read the description

The description has the meat of the information about what the command is and
what it does.  This part is generally what you are looking for when you first
start reading a man page. 

# Skim the options

The options tell you all the different things you can do with this command and
how you do those things.  I recommend skimming this section to see if there are
any specific options that interest you and reading those in detail.   

# Browse the rest for anything that looks relevant

There are  other sections inside a typical man page, and on your first read a
quick skim of these is probably enough to give you an idea of whether this
command can do what you need.  One section that is usually  helpful is the
EXAMPLES section.  This will show some examples of how to properly use the
command you are reading about.  Not all commands will have an EXAMPLES section,
however. 

Homework
========
1. Use the ls man page to figure out what this does: 

```ksh
$> ls -larth
```

2. Read the man page of another of the commands we've recently looked at (pwd,
   whoami, man)

New Terms
=========
- 'man page' - What the system manuals are generally called, as in: "Did you
  read the `ls` man page?"

- `man` - The command to load a man page for a given command

[^1]: http://man.openbsd.org/OpenBSD-current/man1/man.1
