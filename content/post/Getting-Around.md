+++
bigimg = ""
date = "2016-09-26T01:55:24-04:00"
subtitle = "cd, cd .., cd -, and more."
title = "Getting Around"
postDate = "2016-09-26"
+++

When you first start using the CLI, you will spend most of your time navigating
the file system, looking for things.  In this post I'll give you some tips that
should make this process more enjoyable. 

When you first open the terminal program you should automatically be in your
home directory.  Your home directory is where all your files live. 

You can verify this by issuing the `pwd` command.  You should
see something that looks like this: `/home/gabe` or `/Users/gabe` if you are on
a Mac.  If that's not what you see, there are a few ways to get there.

You can use the `cd` command and specify the full path to your home directory
like this: 

```ksh
$> cd /home/gabe
```

Instead of specifying the full path to your home directory, you can use the
special shortcut `~/`. The shell automatically expands that to the full path of
your home directory.  

```ksh
$> cd ~/
```

Another shortcut for getting back to your home directory is just calling `cd`
without any arguments: 

```ksh
$> cd
```

If you ever find yourself lost, and don't know how to get back to your files,
any of those options will do the trick.


While navigating around there are a few special directories that exist in every
directory.  The "." (dot) and ".." (dot dot) directories always refer to the current directory
and the directory one level up the tree, respectively. 


For example, if you consider this directory tree: 

```ksh
/
|-- home
    |-- gabe
	    |-- Documents
```

And you then `cd` into the Documents directory `cd /home/gabe/Documents` and
issue the `ls` command you would see something like this: 

```
20120409-endo.pdf
20120601-tres.pdf
Bologna-Hotel-Res.ps
Bologna-Hotel-Reservation.pdf
Brochure-English-web.pdf
CAD
DailyBook
```

In order to see the "special" directories as well, I need to pass the `-a` flag
to the ls command, like so: `ls -a`.   Then I will see something more like this: 

```
.
..
.login.conf.swp
.stfolder
20120409-endo.pdf
20120601-tres.pdf
Bologna-Hotel-Res.ps
Bologna-Hotel-Reservation.pdf
Brochure-English-web.pdf
CAD
DailyBook
```

You'll notices the directory listings look almost the same, except for the first
few entries.  When I pass the `-a` flag to the `ls` command, I'm asking it to
"Include directory entries whose names begin with a dot ('.')."

File names that start with a '.' are usually called "hidden files" since
they don't show up when you just run the bare `ls` command.  They also won't
show up in your GUI (graphical user interface) when you are browsing around
unless you have specifically told your system to show hidden files.  The two
hidden files that will appear time and time again are "." and "..".  

".." can be used to go up a level in the directory tree.  Given I'm in the directory
`/home/gabe/Documents` and I want to go back up to `/home/gabe` I can just type:
`cd ..` and I will be moved up one level in my directory structure to
`/home/gabe`.  I can keep doing this all the way back to the `/` (root)
directory.  

```ksh
$> pwd 
/home/gabe/Documents
$> cd ..
$> pwd
/home/gabe
$> cd ..
$> pwd
/home
$> cd ..
$> pwd
/
```

One last quick tip for today, if you've just changed into a directory but want
to go back to where you just came from, you can type `cd -` (cd dash).  This will jump you
back and forth between the directory you are currently in, and the directory you
were previously in.  For example: 

```
$> cd /tmp
$> pwd
/tmp
$> cd -
$> pwd
/home/gabe
$> cd -
$> pwd
/tmp
$> cd - 
$> pwd
/home/gabe
```

That's it for this time.  If you have any questions, feedback, or suggestions
for topics you'd like to see covered, just drop me an email: <a
href="mailto:gabe@cotcli.com">gabe@cotcli.com</a>

New Terms
=========
- . - A single dot or period, means the current working directory

- .. - Two dots, means the directory one level above the current working
  directory

- ~/ - Tilde slash is a shortcut for your home directory, where all your
  documents live

- hidden file - A file that begins with a single dot, for example: .hiddenfile
  that won't show up when you run the `ls` command, unless you also specify the
  `-a` flag.
