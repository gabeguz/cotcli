+++
title = "Size Date Name"
subtitle = "The end of ls"
date = "2017-02-20T16:38:16-05:00"
bigimg = ""
+++

When I first started writing about the output of the `ls` command, I didn't
expect it would take four posts to get through, but here we are.  We're almost
to the end, the only things left to talk about are the file size, the last
modified time, and the filename.

The file size is represented in bytes, unless you pass the `-h` flag to `ls` in
which case, the size will be displayed in Kilobyte, Megabyte, Gigabyte,
Terabyte, Petabyte, and Exabyte, depending on how much stuff you have!    


For example: 

```ksh
$ cd Downloads
$ ls -lrth
-rw-r--r--  1 gabe  gabe   3.1G Sep 18  2014 loebolus-data-master.zip
-rw-r--r--  1 gabe  gabe   200K Sep 20  2014 bootstrap-3.2.0-dist.zip
-rw-r-----  1 gabe  gabe   3.1M Sep 21  2014 phpunit.phar
-rw-r-----  1 gabe  gabe   153B Sep 21  2014 submit.php
```
Here we see (G)igabytes, (K)ilobytes, (M)egabytes, and (B)ytes respectively.

We can also see that these files were last modified in September of 2014.  If I
were to make a change to any of those files, or use the `touch` command to
update their modified time to now, that would be reflected in the output of
`ls`: 

```ksh
$ touch phpunit.phar
$ ls -lrth
-rw-r--r--  1 gabe  gabe   3.1G Sep 18  2014 loebolus-data-master.zip
-rw-r--r--  1 gabe  gabe   200K Sep 20  2014 bootstrap-3.2.0-dist.zip
-rw-r-----  1 gabe  gabe   153B Sep 21  2014 submit.php
-rw-r-----  1 gabe  gabe   3.1M Feb 13 22:12 phpunit.phar
```
Finally, we have the name of the file that we're looking at.  Hopefully that one
is self-explanatory!

That concludes the *long* tour of the `ls` command.  Next time I'll be sharing
some tips and tricks for using your shell more efficiently.

New Terms
=========
`ls -lrth`
: list the contents of the directory in (l)ong format, (r)everse order, sorted
by (t)ime, with sizes in (h)uman readable format.  This is probably my most
frequently used format of the `ls` command, as it shows me the most recently
changed files at the bottom of the screen.

Bit
: a one or a zero

Byte
: 8 Bits

Kilobyte
: 1024 Bytes

Megabyte
: 1024 Kilobytes

Gigabyte
: 1024 Megabytes

Terabyte
: 1024 Gigabytes

Petabyte
: 1024 Terabytes

Exabyte
: 1024 Petabytes

There are multiple notations for Kilobytes etc, which you can read more about
[here][0].

[0]: https://en.wikipedia.org/wiki/Kilobyte
