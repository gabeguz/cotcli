+++
bigimg = ""
date = "2016-06-06T15:15:20-04:00"
subtitle = "Where am I?"
title = "The ls Command And Basic File System Structure"
publishdate = "2016-06-06"
description = "A quick introduction to the ls command."
+++

Hello again, 

Today we'll continue with another command that you can type into your terminal.
We've already seen that `pwd` (print working directory) can tell us where we
are, but what about if we want to see our files? 

The `ls` (list) command does just that: 

```ksh
$> ls
dead.letter mbox        rtsignal.c    src
```

This lists the files and folders that are in the directory that I'm currently
in: `/home/gabe`.   I can confirm I'm in the folder I think I'm in by
re-issuing the `pwd` command: 

```ksh
$> pwd
/home/gabe
```

If I want more details on those files I can do this:

```ksh
$> ls -l
total 3056
-rw-------  1 gabe  gabe        7 Feb  7  2014 dead.letter
-rw-------  1 gabe  gabe  1525103 Oct 15 22:05 mbox
-rw-r-----  1 gabe  gabe     3337 Nov  5 15:11 rtsignal.c
drwxr-xr-x  2 gabe  gabe      512 Nov 10 21:22 src
```

Here I called the list program (`ls`), and then I added a `-l` (dash ell) after
the command.  This is how you specify an option.  The `-l` (dash ell) option
tells the `ls` program that I would like the 'long' or detailed list of files.  

This gives me extra information about each file.  I won't go into all the
specifics now, but something useful is the 'd' in the first column of the last
line. This tells me that the `src` entry is actually a directory (folder),
while all the rest of them are files. We will cover the rest of the information
provided in a future post.  

We'll take a small detour now to talk a bit about file system organization on a
UNIX system.  

The long and the short of it is, you have one main folder, called 'the root
directory' represented by a single slash `/`.  Inside that folder you can have
as many sub-folders as you like, and each of those can have sub-folders as
well.   I'm calling them folders as that's how most people from a graphical
background know them, but in the command line world, we call folders
directories.   

On most UNIX systems, there are several 'system folders' that come
pre-installed, if you issue the following commands you should see some of them.  

First, we will `cd` 'change directory' to the root folder (`/`): 

```ksh
$> cd /

```
There is no output after I type this command, this is normal.  

Then, ask for the long directory listing (`ls -l`): 

```ksh
$> ls -l
total 45556
drwxr-xr-x   2 root  wheel     1024 Nov  7 12:22 bin
-rw-r--r--   1 root  wheel    69660 Nov  8 22:24 boot
-rw-r--r--   1 root  wheel  9962236 Nov  8 21:24 bsd
drwxr-xr-x   3 root  wheel    19456 Nov  8 22:25 dev
drwxr-xr-x  64 root  wheel     3584 Nov  9 15:45 etc
drwxr-xr-x   3 root  wheel      512 Aug 13  2013 home
```

I've only included a sampling of what's in my `/` directory, but it's enough to
be instructive w/out being overwhelming (I hope).   

Here we can see several sub-folders (bin, dev, etc, and home) and two files
(bsd and boot).  Each of these folders can have sub-folders as well.  For
example, if I change into the home folder: 

```ksh
$> cd home

```
and re-issue the list command with the long listing option (`-l`): 

```ksh
$> ls -l
total 56
drwxr-xr-x    9 ariadne  ariadne  512 Sep 28 09:10 ariadne
drwxr-xr-x  130 gabe   gabe    7680 Nov 10 21:30 gabe
```

I see two folders, `ariadne` and `gabe`.  These correspond to users that exist
on my system and are what we call those user's 'home directories.' To write out
the full path to ariadne's home directory, I would write: 

`/home/ariadne` and say: "slash home slash ariadne"

That's our detour, now back to the `ls` (list) command.  

There are other options we can send to the `ls` command depending on what we'd
like it to do.  Most programs in the UNIX world have options that are almost
always specified in the same way, a dash followed by a letter.  Some examples
from the `ls` command (you should follow along with these in your terminal): 

```ksh
$> ls -1
``` 
(dash one) This forces the output of the ls command to be one entry per line.

```ksh
$> ls -a
```
(dash a) This tells the ls command to list all files, including those that
begin with a '.' which are normally hidden (more on that later)

```ksh
$> ls -F
```
(dash capital eff) This tells the ls command to make the type of file more
obvious, by for example appending a `/` after any name that is a folder
(directory).

Oftentimes, several options may be combined as well, as in the case of: 

```ksh
$> ls -lh
```
(dash ell aitch) Which tells ls to give us a 'long' listing, and also to use
the 'h' option to represent the sizes of the files in "human readable" form.
If you do this, you should see a letter after the file size representing
(B)ytes, (K)ilobytes, (M)egabytes, and (G)igabytes. 

Next time we'll look at how to find out which options a particular command
understands, before getting back to more useful stuff.  

New Terms
=========
- directory - folder
- `ls` - list directory contents
- `ls -l` - long listing of directory contents
- `ls -1` - list contents of directory, one per line
- `ls -a` - list all contents of directory, including hidden files
- `ls -lh` - list all contents of directory, and make sizes human readable. 
- `ls -F` - Add some extra identifying information to each directory item
  listed to make it easier to tell the difference between a file and directory.
- `man ls` - Read the manual for the `ls` command
