+++
date = "2016-10-16T15:45:24-04:00"
publishDate = "2016-10-16"
bigimg = ""
title = "Files, Permissions, Binary"
subtitle = "That escalated quickly"
+++

We've spent a fair amount of time poking around on our systems and we've seen
plenty of files and directories while using the `ls` command.  Today we're going
to dig a bit deeper into the concept of files and what to do with them.

First, lets create a new directory to work in.  Be sure you are in your home
directory, and then use the `mkdir` (make directory)  command to create a
directory called "cotcli" (or whatever else you'd like to call it): 

```ksh
$ cd
$ mkdir cotcli
$ cd cotcli
$ pwd
/home/gabe/cotcli
```
Great, now lets create a file here.  You can use the `touch` command to create a
new empty file:

```ksh
$ touch a-new-file
$ ls -l
total 0
-rw-r--r--  1 gabe  gabe  0 Oct 11 15:35 a-new-file
```

Using the '-l' option to the `ls` command gives us a fair amount of information
about the file we just created. 

In order, we have "total 0" this is the total size (in blocks, which are 512
bytes each, let's just ignore that for now) of the files in this directory.  

Then we have a column that looks like this:

```ksh
-rw-r--r-- 
```

This represents the file "mode" for this file, and encodes information such as
what different users are allowed to do to this file, and what type of file it
is.  In the first position we have a "-" which means this is a "regular file"
other things this could be are "d" for "directory", "l" for "symbolic link", "b"
for block special file, etc.  All the different types are referenced in the
`ls(1)` manual page. The next 9 positions represent the permission information
for this file: `rw-r--r--`

Those 9 positions are really 3 sets of 3 permissions broken down as follows:
Three for the owner, three for the group, and three for the world.  The owner is
generally the user that created the file (in this case you), the group is
generally the primary group of the user that created the file (we'll cover
groups in more detail later), and the world refers to all other users on the
system.  So, for this particular file we have:  

```
Owner: rw-  (read and write permissions)
Group: r--  (read permission only)
World: r--  (read permission only)
```

The permissions are always listed in this order \(r)ead, (w)rite, e(x)ecute.  And
the sets of permissions are always listed in owner, group, world order as well.  

Since I created this file (or you, in your case) I'm the owner and I am allowed
to both read and write to this file.  I can also erase this file since I own it,
but we won't worry about that now. 

One of the things I find interesting about the file permissions is that they are
one of the places where a normal user can peek into the inner workings of the
computer.  Each permission can be thought of as 3 bits (a zero or a one) each
bit determining if that permission is set or not.  

```
decimal		binary		english
---------------------------------------
0		000		no permissions
1		001		execute only
2		010		write only
3		011		write and execute only
4		100		read only
5		101		read and execute only
6		110		read and write only
7		111		read, write, and execute
		rwx
```

The binary column shows where each permission is enabled (a 1) or disabled (a 0).
The decimal column shows what the binary number for that set of permissions
corresponds to in decimal, and the english column is the english explanation of
what those 0's and 1's mean.  So, instead of saying that my file has user write
and read permissions, group read permissions, and world read permissions, I can say
the file's mode is: 644


If you've ever heard one of your computer nerd friends say something like, "Oh,
I see the problem, the file is 770 when it should be 775", this is what she was
talking about.

Here is another table that tries to show the relationship: 

```
        rwx
-----------	   
Owner | 110  (in decimal: 6)
Group | 100  (in decimal: 4)
World | 100  (in decimal: 4)
```

I've actually told a bit of a lie, the decimal numbers here are really octal
numbers (base 8) but to keep things simple for the time being we will just
pretend they are decimals.

Next week, we'll finish going through the rest of the information the ls command
provides on our newly created file as well as talking about the e(x)ecute
permission, and what that does.

New Terms
=========
- `touch` - Technically this command is for updating the last time a file was
  accessed or changed, but a side effect of this command is that if the file you
  are "touching" doesn't exist, it will be created.  `man 1 touch` for more
  information.
- permissions - The rules that govern who is allowed to do what to a file.  
- owner - The person who owns a given file, usually the user who created that file.
- group - Users can be lumped into groups and then that group can be given
  permissions to different files.
- world - On Unix systems this referes to "anyone" with an account on the system.  
- binary - base 2 number system where the only possible values are 0 or 1.
- octal - base 8 number system where the only possible values are from 0 to 7.
