+++
title = "Users and Groups"
subtitle = "who are these people?"
date = "2017-02-13T07:34:05-05:00"
postDate = "2017-02-13T07:34:05-05:00"
bigimg = ""
categories = [ "Introduction" ]
tags = [ "users", "groups", "daemon", "server" ]
commands = [ "id", "groups", "cat", "less" ]
+++

Moving on to the next bit of information that's encoded in the `ls` command's
output, we get to the user and group section.  This shows us the owner of the
file, and the primary group associated with the file:  

```ksh
$ ls -l
total 4
-rw-r--r--  2 gabe  gabe    0 Nov  2 13:37 a-hard-link-to-a-new-file
drwxr-xr-x  2 gabe  gabe  512 Dec  5 13:30 a-new-dir
-rw-r--r--  2 gabe  gabe    0 Nov  2 13:37 a-new-file
lrwxr-xr-x  1 gabe  gabe   10 Nov  8 11:36 a-symbolic-link-to-a-new-file ->
a-new-file
```
In this case, the user and the group are the same: `gabe	gabe` 

That is, there is a user on my system named 'gabe' and there is a group on my
system with the same name 'gabe'.  The user listed here is known as the "owner"
of this file.  That means, that I have full control over the "mode" of these
files.  We looked at modes [in an earlier post][modes]. I can change the
permissions, even to the point of making the file unreadable to myself (though I
can always change it back since I'm the owner).  One thing that I can't do is
change the owner of the file to be someone other than myself.  Only the root
user can change the ownership of a file.

The group of the file allows you to specify additional permissions for a group
of users.  


On your system, that will probably look
different.  

There are many users on your average UNIX like operating system, to see some of
them you can look at the /etc/passwd file: 

```ksh
$ cat /etc/passwd
```

`cat (1)` is a command to view the contents of a file.  'cat' stands for
conCATenate as it can be used to concatenate several files together into one
larger file.  For now, we'll just use it to output the contents of a file to the
screen.  We could also use the `less` or `more` pagers to look at the passwd
file: 

```ksh
$ less /etc/passwd
$ more /etc/passwd
```

Remember, you can press `q` at any time to exit a pager, `space` to go to the
next page, `j` to scroll down one line, and `k` to scroll up by one line.  

Here is a selection from my passwd file, in case yours doesn't have much in it: 

```ksh
root:*:0:0:Charlie &:/root:/bin/ksh
daemon:*:1:1:The devil himself:/root:/sbin/nologin
operator:*:2:5:System &:/operator:/sbin/nologin
bin:*:3:7:Binaries Commands and Source:/:/sbin/nologin
sshd:*:27:27:sshd privsep:/var/empty:/sbin/nologin
_portmap:*:28:28:portmap:/var/empty:/sbin/nologin
_identd:*:29:29:identd:/var/empty:/sbin/nologin
...
gabe:*:1000:1000:Gabriel Guzman:/home/gabe:/bin/ksh
```
That information is: 

`username:password:user id:primary group id:name:home directory:default shell`

The password isn't stored in this file, the field just contains a \* for all
users.  The actual encrypted passwords are stored in /etc/master.passwd (or
/etc/shadow on Linux), which is only readable by the root or
administrator account. 

On MacOS, the passwd file is only used when you are running in what is called
"single user mode" (we'll cover that at a later date).  Most of the time, your
Mac gets user information from a database called [Open Directory][0] an LDAP
implementation.  

Why do I have all these users on my computer?
=============================================

Once upon a time, most services on a unix like computer ran as the root user.
We'll talk more about the root user in a later post, but for now, you can think
of the root user as the user that is allowed to do *anything*, no questions
asked.  After awhile, people began to notice that having systems running as the
root user meant that anyone who was able to exploit those systems could easily
gain root access to a machine, effectivly elevating their own privileges or in
modern parlance "owning the machine."  This is not desirable, and so little by
little system services began to be executed as non priviledged users with
limited access to the system.  Some of the users you might recognize are:


```
_postgresql:*:503:503:PostgreSQL Manager:/var/postgresql:/bin/sh
```
The user that runs the [PostgreSQL][1] database server.

```
_ftp:*:84:84:FTP Daemon:/var/empty:/sbin/nologin
```
The user that the venerable [File Transfer Protocol][2] server runs as.

```
_ntp:*:83:83:NTP Daemon:/var/empty:/sbin/nologin
```
The user that the [Network Time Protocol][3] server runs as.

```
www:*:67:67:HTTP Server:/var/www:/sbin/nologin
```
The user that runs the [HTTP][4] server.


Those users are all there so that the servers (also known as daemons) with the
same names can run as normal users and not require root access to the machine.
This way, if someone manages to compromise your ftp server, they don't
automatically get administrator level privileges to your machine.

To find out what groups your user belongs to, you can either user the `groups`
or `id` commands:

```
gabe@xps:~$ groups
gabe wheel wsrc www dialer
gabe@xps:~$ id
uid=1000(gabe) gid=1000(gabe) groups=1000(gabe), 0(wheel), 9(wsrc), 67(www),
117(dialer)
```
 
New Terms
=========
`id`
: Command that returns information about the current logged in user

`groups`
: Command that returns information about the groups the current user belongs to

server
: A program that is always running, waiting for incoming connections.  The web
server, for example waits for a browser to make a request for an html page, and
then serves up that page to the browser.

daemon
: Another name for a server.

root user
: The administrator user.  The root user can do anything on a system.

`cat`
: A program to conCATenate files.  Commonly used to dump the contents of a
single file to the terminal.

`less`
: A program known as a 'pager' which allows you to look at lengthy content one
page at a time.

`more`
: Another 'pager' program.  On some systems, `more` and `less` are the same
program.


[0]: https://en.wikipedia.org/wiki/Apple_Open_Directory
[modes]: https://www.cotcli.com/post/Files-Permissions-Binary/
[1]: https://www.postgresql.org/
[2]: https://en.wikipedia.org/wiki/File_Transfer_Protocol
[3]: https://en.wikipedia.org/wiki/Network_Time_Protocol
[4]: https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol
