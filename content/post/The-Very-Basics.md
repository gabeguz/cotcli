+++
date = "2016-05-23T16:17:26-04:00"
title = "The Very Basics"
publishdate ="2016-05-23"
+++

Welcome to Comfort on the Command Line.  

I'll be writing a series of posts to help you gain comfort with the UNIX
command line interface (cli).  If you're already a cli wizard, then this may
not be the guide for you.
  
What is the command line interface (cli)? 

There are different ways to interact with a computer.  The most common is the
graphical user interface (gui) which you probably use every time you use a
computer.  Graphical user interfaces usually have things like windows, icons
and menus.  The main way to interact with a graphical user interface is with
the mouse, though keyboard shortcuts are also supported.  

Another way to interact (interface) with a computer is by typing a command on
one line, pressing enter, waiting for the computer to run that command, and
seeing the output of that command on the next line.  This is how a command line
interface works.  You type a command, hit return, wait for the computer to
execute that command, and then see the computer's response.   If you remember
DOS, then you know what a command line interface (cli) looks like.  

When you first enter the command line interface, you will see something that
looks a bit like this: 

```ksh
$> 
```

We call this `$>` thing the prompt, as in - the computer is prompting you to do
something... *anything*.  Not that.  Not that either, no no - don't you know what
I need?  Oh right, you don't, how could you I'm just a prompt.

Where was I?  Oh right, the prompt.  Where can I find this prompt?  You should
have a terminal program somewhere on your system.   On a Mac it is actually
called Terminal, and it's in the Utilities folder inside your Applications
folder (notice the icon even has a prompt!).  On Linux systems, there are
several terminal programs available and due to the vastness of the Linux
ecosystem, you should probably type this into your favourite search engine right now: 

"xxx Linux how to get to the terminal" 

Where xxx is your particular Linux distribution (Ubuntu, RedHat, CentOS,
Gentoo, etc).  

If you are running a BSD (I’m impressed and think you are cool - I also run a
BSD) then there will also be various possible ways to get to a terminal program
and you should do the same search I recommend for the Linux people, except
replace "Linux" with "BSD" and xxx with your particular flavor of BSD (Open,
Free, Net, Dragonfly).

Even Windows has a command line interface, and you can run it by typing `cmd`
in the "run program" dialog from the start menu (assuming Windows still has a
start menu).  This is the last mention we will make of Windows since this guide
is about the UNIX cli and Windows doesn't have one of those.  

Ok, first homework assignment.  Go find your terminal program.  Figure out how
to open it, close it, resize it, etc.

That's enough for today, stay tuned, in our next episode I'll actually show you
some things you can *do* with your new found terminal program.  In the mean
time -- feel free to just keep the window open somewhere on your system, this
will impress people.   

p.s. At the end of these posts I'll recap new terms

New Terms
=========
- cli - command line interface

- gui - graphical user interface

- terminal - a program that allows you to interact with the computer via the
  command line interface

- prompt - the weird `$>` looking thing that means the computer is waiting for
  you to type something.

p.p.s. There's another program involved as well, it's called the shell, but I'm
just going to ignore that for now.
