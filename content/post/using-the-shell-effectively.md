+++
date = "2017-02-27T22:25:53-05:00"
publishDate = "2017-02-26T12:25:53-05:00"
bigimg = ""
title = "Using The Shell Effectively"
subtitle = "How to win friends, and influence people (not really)."
categories = ["Tips & Tricks"]
tags = ["cheatsheet", "bash", "ksh", "shell", "tips", "tricks"]
commands = ["env", "grep", "banner", "ls", "bash", "ksh"]
tracks = ["intro", "beginners"]
+++

Once you get comfortable poking around with your shell, you start to wonder if
there aren't easier ways to do some common tasks.  We're constantly `cd`'ing
places, and running commands, and having to remember really long incantations
just to list our files in reverse chronological order (`ls -lrth`).

I've compiled the following list of useful shortcuts for using the command line.
They have all been tested on `bash(1)` and `ksh(1)`.  bash (Bourne Again SHell)
is the default shell on MacOS and many Linux distributions.  ksh (public domain
Korn shell) is the default on OpenBSD, which happens to be what I use.  There
are many shells available, and we'll look at more of them later, for now, I'll
stick to these two.  To see what shell you are running, you can type: 

```ksh
$ env |grep SHELL
SHELL=/bin/ksh
```

Movement
========
The idea that started this blog.  How to I get to the beginning of the line?
Here's the answer to that as well as a few more movement tips:

`ctrl + a`
: Move the cursor to the beginning of the line

`ctrl + e`
: Move the cursor to the end of the line

`esc + f`
: Move the cursor forward one word

`esc + b`
: Move the cursor back one word

Just Kidding
============
Sometimes you don't want to run what you just typed, how do you erase it? 

`ctrl + u`
: Erase everything you've typed on the line, and move the cursor to the beginning

`ctrl + k`
: Delete from the cursor to the end of the line

`esc, backspace`
: Erase the last word you typed

Tab Completion
==============
Can't remember what the command you're looking for is, but you know it starts
with the letters ba? Type `ba` followed by the \<tab\> key.

```ksh
$ ba<tab>
backgammon   badsect      banner       basename     batch        battlestar
```
Oh, right, `banner`: 

```ksh
$ banner hello
 #    #  ######  #       #        ####
 #    #  #       #       #       #    #
 ######  #####   #       #       #    #
 #    #  #       #       #       #    #
 #    #  #       #       #       #    #
 #    #  ######  ######  ######   ####
```

This is also super useful if you're just lazy.  Don't want to type `ls`?  Just
type `l` followed by \<tab\>!  (OK, that doesn't help at all.)

This can also be used on file names: 
```ksh
$ cd cotcli
$ ls
a-hard-link-to-a-new-file               a-symbolic-link-to-a-new-file
a-new-dir                               file.txt
a-new-file
$ cp f<tab>
$ cp file.txt new.txt
```

Tab will show you the list of things that matches, until there is only one
thing left that can match, and then it will leave that there for you.

Find a command you previously ran
=================================
Did you just type out some really long complicated command, and now you have to
do it again, but don't want to type the whole thing?  `ctrl + p` (or up arrow)
to the rescue.  Hitting `ctrl + p` once will bring up the most recent command
you ran, and continuing to press that combination will cycle through your
command history until you get to the end (beginning).  `ctrl + n` (or down
arrow) will do the opposite, assuming you've already gone back in time.  That
is, pressing this before you've pressed `ctrl + p` won't do anything.  You can
also *search* through your command history with `ctrl + r` then type a few
letters from a previous command and watch the magic.

`ctrl + p`
: Previous history entry

`ctrl + n`
: Next history entry

`ctrl + r`
: Search for something you previously ran, after pressing this, just start typing
and see what happens.  Once you have a match, you can continue hitting `ctrl +
r` to iterate through commands that match what you've already searched for.


Do That Again
=============
`!!`
: Run the previous command again (does not work in `ksh`)


New Terms
=========
(Basically this whole post was new terms, but here are a few extras)

shell
: A program that allows you to execute commands.  Up until now we've been using
the terms 'shell', 'terminal', and 'command line' somewhat interchangeably.  In
reality they are 3 different things.


bash
: The (B)ourne (A)gain (SH)ell.  A very popular shell that is the default on
MacOS and many Linux distributions.

ksh
: The (K)orn (SH)ell.  A less common shell that used to be the default on MacOS
and is currently the default on OpenBSD.

`env`
: A program to set and print environment. `man env` for more info.

`grep`
: A program to search for text. `man grep` for more info.

`banner`
: A program to print text as a "banner" (I used to have a printout of
my name made on an old dot matrix printer using this program.  My uncle made it
for me when I visited his work back in the 80s.  It was very cool.)

`cp`
: A program to copy one file to another.  `man cp` for more info.
